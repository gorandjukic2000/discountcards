import 'cards.dart';

void main() {
  try {
    var cards = [
      Bronze(0, 150, "Goxy"),
      Silver(600, 850),
      Gold(1500, 1300),
      Gold(499, 1300)
    ];

    for (Card card in cards) card.PrintOutput();
  } catch (ex) {
    print("Exception caught: $ex");
  }
}
