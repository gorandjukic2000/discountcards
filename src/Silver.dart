part of cards;

class Silver extends Card {
  Silver(_turnover, _valueOfPurchase, [_owner])
      : super(_turnover, _valueOfPurchase, _owner);

  @override
  void CalculateDiscountRate() {
    _discountRate = 2;
    if (_turnover > 300) _discountRate = 3.5;
  }
}
