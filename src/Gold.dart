part of cards;

class Gold extends Card {
  Gold(_turnover, _valueOfPurchase, [_owner])
      : super(_turnover, _valueOfPurchase, _owner);

  @override
  void CalculateDiscountRate() {
    _discountRate = 2;
    if (_turnover >= 800)
      _discountRate = 10;
    else
      _discountRate += (_turnover / 100).floor();
  }
}
