part of cards;

abstract class Card {
  final String _owner;
  //String get Owner => _owner;
  num _turnover;
  //num get Turnover => _turnover;
  num _discountRate = 0;
  num _valueOfPurchase;

  Card(this._turnover, this._valueOfPurchase, [this._owner]) {
    CalculateDiscountRate();
  }

  void CalculateDiscountRate();
  num CalculateDiscount() => _valueOfPurchase * (_discountRate / 100);
  num CalculateTotal() => _valueOfPurchase - CalculateDiscount();

  void PrintOutput() {
    if (this._owner != null) print("Owner: ${this._owner}");
    print("Purchase value: ${this._valueOfPurchase}");
    print("Discount rate: ${this._discountRate}");
    print("Discount: ${this.CalculateDiscount().toStringAsFixed(2)}");
    print("Total: ${this.CalculateTotal()}");
    print("");
  }
}
