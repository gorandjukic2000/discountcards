part of cards;

class Bronze extends Card {
  Bronze(_turnover, _valueOfPurchase, [_owner])
      : super(_turnover, _valueOfPurchase, _owner);

  @override
  void CalculateDiscountRate() {
    if (_turnover > 100) _discountRate = 1;
    if (_turnover > 300) _discountRate = 2.5;
  }
}
